import * as fs from "fs"
import * as path from "path"
import chapter1 from "./chapter1"
import speaker from "./speaker"

const DIR_VNDAT = "vndat"
const DIR_UNPACKED = "unpacked"
const DIR_PATCHED = "patched"

class ArchivedFile
{
    path: string
    offset: number
    size: number

    constructor(path: string, offset: number, size: number)
    {
        this.path = path
        this.offset = offset
        this.size = size
    }
}

const writeFile = (filePath: string, fileData: string | Buffer) =>
{
    const dir = path.dirname(filePath)

    if (!fs.existsSync(dir))
    {
        fs.mkdirSync(dir, { recursive: true })
    }

    fs.writeFileSync(filePath, fileData)
}

function* getArcPaths(dirPath: string): Generator<string>
{
    for (const ent of fs.readdirSync(`${DIR_PATCHED}/${dirPath}`, { withFileTypes: true }))
    {
        const arcPath = `${dirPath}/${ent.name}`

        if (ent.isDirectory())
        {
            yield* getArcPaths(arcPath)
        }
        else
        {
            yield arcPath
        }
    }
}

const pack = () =>
{
    const arcs: ArchivedFile[] = []

    for (const dir of fs.readdirSync(DIR_PATCHED))
    {
        for (const arcPath of getArcPaths(dir))
        {
            arcs.push(new ArchivedFile(arcPath, 0, 0))
        }
    }

    const buffers = new Set<Buffer>()
    let offset = 0

    for (let i = 0; i < arcs.length; i++)
    {
        const arc = arcs[i]
        console.log(`[Pack] Data ${i + 1}/${arcs.length} - ${arc.path}`)

        const header = Buffer.from([
            0x50, 0x4B, 0x03, 0x04,
            0x2D, 0x00, 0x08, 0x08,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00,
            0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF,
            0x00, 0x00, // file name length uint16
            0x14, 0x00,
            ...Buffer.from(arc.path, "ascii"),
            0x01, 0x00, 0x10, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00
        ])

        header.writeUInt16LE(arc.path.length, 26)

        const contents = fs.readFileSync(`${DIR_PATCHED}/${arc.path}`)
        xorBuffer(contents)

        arc.size = contents.length
        arc.offset = offset

        const footer = Buffer.from([
            0x50, 0x4B, 0x07, 0x08,
            0x00, 0x00, 0x00, 0x00, // ??? uint32
            0x00, 0x00, 0x00, 0x00, // file size uint32
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, // file size uint32
            0x00, 0x00, 0x00, 0x00
        ])

        footer.writeUInt32LE(contents.length, 8)
        footer.writeUInt32LE(contents.length, 16)

        buffers.add(header)
        buffers.add(contents)
        buffers.add(footer)

        offset += header.length + contents.length + footer.length
    }

    for (let i = 0; i < arcs.length; i++)
    {
        const arc = arcs[i]
        console.log(`[Pack] Index ${i + 1}/${arcs.length} - ${arc.path}`)

        const index = Buffer.from([
            0x50, 0x4B, 0x01, 0x02,
            0x00, 0x00, 0x14, 0x00,
            0x08, 0x08, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, // ??? uint32
            0x00, 0x00, 0x00, 0x00, // file size uint32
            0x00, 0x00, 0x00, 0x00, // file size uint32
            0x00, 0x00, // file name length uint16
            0x28, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00,
            0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, // file offset uint32
            ...Buffer.from(arc.path, "ascii"),
            0x51, 0x1A, 0x24, 0x00,
            0x17, 0x00, 0x20, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ???
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ???
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ???
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 // ???
        ])

        index.writeUInt32LE(arc.size, 20)
        index.writeUInt32LE(arc.size, 24)
        index.writeUInt16LE(arc.path.length, 28)
        index.writeUInt32LE(arc.offset, 42)

        buffers.add(index)
    }

    const indices = Buffer.from([
        0x50, 0x4B, 0x05, 0x06,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, // num files uint16
        0x00, 0x00, // num files uint16
        0x51, 0x1A, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, // first index offset uint32
        0x00, 0x00
    ])

    indices.writeUInt16LE(arcs.length, 8)
    indices.writeUInt16LE(arcs.length, 10)
    indices.writeUInt32LE(offset, 16)

    console.log("[Pack] Writing to patch file")
    buffers.add(indices)
    writeFile("patch.vndat", Buffer.concat([...buffers]))
    console.log("[Pack] Done")
}

const setFont = (fontFile: string, size: number) =>
{
    const path = "Scripts/system/textbox.txt"
    let contents = fs.readFileSync(path, { encoding: "utf-8" })

    contents = contents.replace(
        /^(プリーロード \/Fonts\/)\w+\.ttf (永久)$/mu,
        `$1${fontFile} $2`
    )

    contents = contents.replace(
        /^(文字窓0 txtbox 60 610 100 824 600) \w+\.ttf \d+ (カメラ付着)$/mu,
        `$1 ${fontFile} ${size} $2`
    )

    fs.writeFileSync(path, contents)
}

const translateDialogue = (chapterPath: string, translation: string[]) =>
{
    const keepPersonalSuffixes = true
    const honorificSuffixes = /(?:-san|-sama|-kun|-chan)/ug
    const roleSuffixes = new Map([
        [/(?<=^|(?:[.!?—♥]\s))(\w+)-sensei/ug, "Teacher"],
        [/(\w+)-sensei/ug, "teacher"],
        [/(?<=^|(?:[.!?—♥]\s))(\w+)-hakase/ug, "Professor"],
        [/(\w+)-sensei/ug, "professor"]
    ])

    const contents = fs.readFileSync(`${DIR_UNPACKED}/${chapterPath}`, { encoding: "utf-8" })
    const contentsSplit = contents.split(/(?=")|(?<=\\w)/ug)

    for (let i = 0, j = 0; i < contentsSplit.length; i++)
    {
        const line = contentsSplit[i]
        let translated = translation[j]

        if (j < translation.length &&
            line.startsWith("\"") &&
            !line.startsWith("\"\r\n") // fix line 2342 @ chapter1.txt
        )
        {
            if (!keepPersonalSuffixes)
            {
                translated = translated.replace(honorificSuffixes, "")

                for (const [regex, replacement] of roleSuffixes.entries())
                {
                    translated = translated.replace(regex, `${replacement} $1`)
                }
            }

            contentsSplit[i] = `"${translated}\\w`
            j++
        }
        else
        {
            for (const [speakerOrig, speakerTrans] of Object.entries(speaker))
            {
                if (line.includes(speakerOrig))
                {
                    contentsSplit[i] = line.replace(speakerOrig, speakerTrans)
                    break
                }
            }
        }
    }

    writeFile(`${DIR_PATCHED}/${chapterPath}`, contentsSplit.join(""))
}

const unpack = (...arcPaths: string[]) =>
{
    for (const arcPath of arcPaths)
    {
        const buffer = fs.readFileSync(`${DIR_VNDAT}/${arcPath}`)
        const { length } = buffer

        const numFiles = buffer.readUInt16LE(length - 0xE)
        let offset = buffer.readUInt32LE(length - 0x6) // First [50 4B 01 02]
        const archived = new Set<ArchivedFile>()

        for (let i = 0; i < numFiles; i++)
        {
            offset += 0x14
            const fileSize = buffer.readUInt32LE(offset)

            offset += 0x8
            const filePathLen = buffer.readUInt16LE(offset)

            offset += 0xE
            const fileOffset = buffer.readUInt32LE(offset)

            offset += 0x4
            const filePath = buffer.slice(offset, offset + filePathLen).toString()

            archived.add(new ArchivedFile(filePath, fileOffset, fileSize))

            offset += filePathLen + 0x28
        }

        for (const arc of archived)
        {
            console.log(`[Unpack] ${arc.path}`)

            const fileContentsOffset = arc.offset + 0x1E + arc.path.length + 0x14
            const outData = buffer.slice(fileContentsOffset, fileContentsOffset + arc.size)
            xorBuffer(outData)

            writeFile(`${DIR_UNPACKED}/${arc.path}`, outData)
        }
    }
}

const xorBuffer = (buffer: Buffer) =>
{
    const encryptLen = Math.min(buffer.length, 100)
    const encryptKey = "d6c5fKI3GgBWpZF3Tz6ia3kF0"

    for (let i = 0; i < encryptLen; i++)
    {
        const xor = encryptKey.charCodeAt(i % encryptKey.length)
        buffer[i] ^= xor

        if (i)
        {
            buffer[buffer.length - i] ^= xor
        }
    }
}

unpack(
    // "data001.vndat", // BGM
    // "data002.vndat", // Fonts
    "data003.vndat", // Images
    // "data004.vndat", // (empty)
    "data005.vndat", // Scripts
    // "data006.vndat", // SFX
    // "data007.vndat", // (empty)
    // "data008.vndat", // Voices
    // "data009.vndat", // (empty)
    // "data010.vndat", // (empty)
    "data100.vndat", // Plugins
)

// setFont("ookamifont.ttf", 28) // Default
// setFont("ookamifont.ttf", 22) // TODO: find a better font

translateDialogue("Scripts/chapter1.txt", chapter1)

pack()
